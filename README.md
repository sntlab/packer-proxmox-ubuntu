# Packer Proxmox Ubuntu

Packer configurations to create Ubuntu templates on Proxmox

## Run

In order to run the script, run this:

```
packer build -var-file=credentials.pkr.hcl ubuntu-server-2204.pkr.hcl
```
